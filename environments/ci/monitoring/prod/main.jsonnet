local monitoring = import 'services/ci-runners/monitoring/monitoring.libsonnet';

monitoring {
  _config+:: {
    monitoring+: {
      loadBalancerIP: '34.138.163.173',
    },
  },
}
