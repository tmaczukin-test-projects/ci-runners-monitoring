local monitoring = import 'services/ci-runners/monitoring/monitoring.libsonnet';

monitoring {
  _config+:: {
    monitoring+: {
      prometheus+: {
        imageVersion: 'latest',
      },
    },
  },
}
