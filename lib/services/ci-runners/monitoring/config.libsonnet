{
  _config+:: {
    monitoring: {
      namespace: 'monitoring',
      loadBalancerIP: '127.0.0.1',
      prometheus: {
        name: 'prometheus',
        image: 'prom/prometheus',
        imageVersion: 'v2.28.1',
        replicas: 2,
        port: 9090,
      },
    },
  },
}
