local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local namespace = k.core.v1.namespace;
local deployment = k.apps.v1.deployment;
local container = k.core.v1.container;
local port = k.core.v1.containerPort;
local service = k.core.v1.service;

(import './config.libsonnet') + {
  monitoring: {
    prometheus: {
      local c = $._config.monitoring,
      namespace: namespace.new(c.namespace),
      deployment:
        deployment.new(
          name=c.prometheus.name,
          replicas=c.prometheus.replicas,
          containers=[
            container.new(
              c.prometheus.name,
              '%s:%s' % [c.prometheus.image, c.prometheus.imageVersion],
            ) + container.withPorts([
              port.new('api', c.prometheus.port),
            ]),
          ]
        )
        + deployment.metadata.withNamespace(c.namespace)
        + deployment.spec.withMinReadySeconds(10)
        + deployment.spec.withRevisionHistoryLimit(10),
      service:
        k.util.serviceFor(self.deployment)
        + service.spec.withType('LoadBalancer')
        + service.spec.withLoadBalancerIP(c.loadBalancerIP),
    },
  },
}
