ALPINE_VERSION ?= 3.14
GCLOUD_SDK_VERSION ?= 351.0.0
KUBECTL_VERSION ?= v1.21.3
# See more: https://tanka.dev/
TANKA_VERSION ?= v0.17.1
# See more: https://github.com/jsonnet-bundler/jsonnet-bundler/
JSONNET_BUNDLER_VERSION ?= v0.4.0

CI_IMAGE ?= ci-runners-monitoring/ci

.PHONY: build_ci_image
build_ci_image:
	# Build $(CI_IMAGE) image
	@docker build \
		--tag $(CI_IMAGE) \
		--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
		--build-arg GCLOUD_SDK_VERSION=$(GCLOUD_SDK_VERSION) \
		--build-arg KUBECTL_VERSION=$(KUBECTL_VERSION) \
		--build-arg TANKA_VERSION=$(TANKA_VERSION) \
		--build-arg JSONNET_BUNDLER_VERSION=$(JSONNET_BUNDLER_VERSION) \
		-f ./dockerfiles/ci/Dockerfile \
		./dockerfiles/ci/

.PHONY: publish_ci_image
publish_ci_image:
	# Publish $(CI_IMAGE) image
ifneq ($(CI_REGISTRY),)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value; skipping image publication
endif
