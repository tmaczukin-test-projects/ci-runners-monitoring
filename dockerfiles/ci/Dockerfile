ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache \
    bash \
    ca-certificates \
    curl \
    git \
    jsonnet \
    make \
    openssl \
    py-pip

ARG GCLOUD_SDK_VERSION

RUN curl -L "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz" -o /tmp/google-cloud-sdk.tar.gz && \
    tar -xzvf /tmp/google-cloud-sdk.tar.gz -C /usr/local/lib && \
    /usr/local/lib/google-cloud-sdk/install.sh --usage-reporting=false --path-update=true && \
    rm -rf /tmp/*

ENV PATH="/usr/local/lib/google-cloud-sdk/bin:${PATH}"

RUN gcloud --quiet components update

ARG KUBECTL_VERSION

RUN curl -L "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /tmp/kubectl && \
    curl -L "https://dl.k8s.io/${KUBECTL_VERSION}/bin/linux/amd64/kubectl.sha256" -o /tmp/kubectl.sha256 && \
    echo "$(cat /tmp/kubectl.sha256)  /tmp/kubectl" | sha256sum -c && \
    chmod +x /tmp/kubectl && \
    mv /tmp/kubectl /usr/local/bin && \
    kubectl version --client && \
    rm -rf /tmp/*

ARG TANKA_VERSION

RUN curl -L "https://github.com/grafana/tanka/releases/download/${TANKA_VERSION}/tk-linux-amd64" -o /usr/local/bin/tk && \
    chmod +x /usr/local/bin/tk && \
    tk --version

ARG JSONNET_BUNDLER_VERSION

RUN curl -L "https://github.com/jsonnet-bundler/jsonnet-bundler/releases/download/${JSONNET_BUNDLER_VERSION}/jb-linux-amd64" -o /usr/local/bin/jb && \
    chmod +x /usr/local/bin/jb && \
    jb --version