include Makefile.*.mk

developmentClusterName := ci-runners-devlopment

.PHONY: create-development-cluster
create-development-cluster: export KUBECONFIG := $(HOME)/.kube/config
create-development-cluster:
	# Creating development K3D cluster
	@k3d cluster create $(developmentClusterName) \
		--servers 3 \
		--agents 3 \
		--api-port 127.0.0.1:6666 \
		-p 8081:80@loadbalancer \
		-p 39090:9090@loadbalancer

.PHONY: delete-development-cluster
delete-development-cluster: export KUBECONFIG := $(HOME)/.kube/config
delete-development-cluster:
	# Deleting development K3D cluster
	@k3d cluster delete $(developmentClusterName)

.PHONY: tk-fmt
tk-fmt:
	@tk fmt environments/ lib/

.PHONY: check-tk-fmt
check-tk-fmt:
	@$(MAKE) tk-fmt
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files | grep -e '.jsonnet' -e '.libsonnet') && \
		echo "Formatting up-to-date!"
